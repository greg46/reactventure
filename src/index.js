import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route  } from 'react-router-dom';
import './css/index.css';
import App from './component/App';
import Test from './component/Test';
import * as serviceWorker from './serviceWorker';

const Root = () => {
    return (
        <BrowserRouter>
            <div>
                <Route exact path='/' component={App} />
                <Route path='/test' component={Test} />
            </div>
        </BrowserRouter>
    )
}


ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
