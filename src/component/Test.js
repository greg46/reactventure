import React, { Component } from 'react';
import '../css/App.css';

import * as firebase from 'firebase'
import config from '../firebase'

class Test extends Component {
    constructor(){
        super()
        this.state = { loading: true }
        firebase.initializeApp(config)
    }

    componentWillMount () {
        
        
        const testRef = firebase.database().ref('test')
        
        testRef.on('value', snapshot => {
            console.log(snapshot.val())
            this.setState({
                test: snapshot.val(),
                loading: false
            })
        })
    }
    render() {
        
        if (this.state.loading){
            return (
            <p>je charge</p>
            )
        }
        const test = Object.keys(this.state.test).map(key => {
            return <p key={key}>{this.state.test[key].nom }</p>
        })
        return (
           <div>{test}</div> 
        )
    }
}
export default Test