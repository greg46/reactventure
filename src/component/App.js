import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import '../css/App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state={ nameInput : '' }
    this.handleNameInput = this.handleNameInput.bind(this)
  }
  handleNameInput(e){
    this.setState({ nameInput : e.target.value })
  }
  render() {
    return (
      <div>
      <header>
        <h1>Reactventure</h1><br></br>
        <input type="text" onChange={this.handleNameInput} value={this.state.nameInput} id="name" placeholder="Entrez le nom de votre personnage"/>
      </header>
      <div className="container">
      <div className="container test">
        
        <div className="card">
           <img src="http://donjonfacile.fr/ressources/img/user_img/5288/img_5606cc82df68f.png" className="card-img-top" alt="Nain"/>
          <div className="card-body">
            <h5 className="card-title">Nain</h5>
            <p className="card-text">Personnage aux stats défensives élevées.</p>
            <ul>
              <li>Points de Vie : 120</li>
              <li>Attaque : 30</li>
              <li>Defense : 20</li>
              <li>Nom : {this.state.nameInput} </li>
            </ul>
          </div>
        </div>

        <div className="card">
          <img src="http://lemag.macp3.info/n19/images/DEC_bd_naheulbeuk_1.png" className="card-img-top" alt="Humain"/>
          <div className="card-body">
            <h5 className="card-title">Humain</h5>
            <p className="card-text">Personnage aux stats <br></br>équilibrées.</p>
            <ul>
              <li>Points de Vie : 100</li>
              <li>Attaque : 50</li>
              <li>Defense : 10</li>
              <li>Nom : {this.state.nameInput} </li>
            </ul>
          </div>
        </div>

        <div className="card">
          <img src="https://vignette.wikia.nocookie.net/wikidupeuple/images/0/0d/Elfe_Art.png/revision/latest?cb=20101010210259&path-prefix=fr" className="card-img-top" alt="Elfe"/>
          <div className="card-body">
            <h5 className="card-title">Elfe</h5>
            <p className="card-text">Personnage aux stats offensives élevées.</p>
            <ul>
              <li>Points de Vie : 80</li>
              <li>Attaque : 70</li>
              <li>Defense : 5</li>
              <li>Nom : {this.state.nameInput} </li>
            </ul>
          </div>
        </div>
      </div>
      <Link to='/test'>
       <button type="button" className="btn btn-secondary btn-lg" id="valider">Valider</button> 
      </Link>
      </div>
      </div>
    );
  }
}

export default App;

class Personnage extends Component{
  constructor(name){
    super()
    this.name = name
    switch(){
      case 'Humain':
      this.race = new Humain()
      break;
      case 'Nain':
      this.race = new Nain()
      break;
      case 'Elfe':
      this.race = new Elfe()
    }
  }
}

//  class Humain extends Personnage{
//    constructor(props){
//      super(props);
//         this.state = {
//           pv = 100
//           attaque = 50
//           defense = 10
//         }

//    }
//  }

// class Nain extends Personnage{
//   constructor(name){
//     super(name)
//     this.pv = 120
//     this.attaque = 30
//     this.defense = 20
//   }
// }

// class Elfe extends Personnage{
//   constructor(name){
//     super(name)
//     this.pv = 80
//     this.attaque = 70
//     this.defense = 5
//   }
// }